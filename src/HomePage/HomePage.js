import React from 'react';
import './HomePage.css';
import lang from '../lang.json'


class HomePage extends React.Component {
  constructor() {
    super();
  }
  setLang(lang) {
    localStorage.setItem('lang', lang);
  }

  render() {
    
    return (
        <>
        
        <div className="homePage-title">
          <h1>{lang[localStorage.getItem('lang')].title}</h1>
        </div>
        </>
      );
}
}
export default HomePage;
