import React from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import {Nav, Button, Alert } from 'react-bootstrap';
import HomePage from './HomePage/HomePage.js';
import TicTacToe from './TicTacToe/TicTacToe.js';
import PaddleGame from './PaddleGame/PaddleGame.js';
import Settings from './Settings/Settings.js';
import lang from './lang.json';
import './App.css';
import { EventEmitter } from './EventEmitter';

class App extends React.Component {
  constructor() {
    super();


    this.state = {
      settingsChangedOn: null
    }

    if (!localStorage.getItem('lang')) {
      localStorage.setItem('lang', 'en');
    }

    EventEmitter.subscribe('langChange', (event) => {
      console.log('got ya!')
      this.setState({
        settingsChangedOn: new Date()
      })
    })

  }
  
  render() {
    
    return (
        <>
<Router>
  <div className="nav-container">  
  <Nav variant="tabs" defaultActiveKey="/home">
    <Nav.Item>
      <Link className="nav-link" to="/">{lang[localStorage.getItem('lang')].homePage}</Link>
    </Nav.Item>
    <Nav.Item>
      <Link className="nav-link" to="/tictactoe">{lang[localStorage.getItem('lang')].ticTacToe}</Link>
    </Nav.Item>
    <Nav.Item>
      <Link className="nav-link" to="/paddlegame">{lang[localStorage.getItem('lang')].paddleGame}</Link>
    </Nav.Item>
    <Nav.Item>
      <Link className="nav-link" to="/settings">{lang[localStorage.getItem('lang')].settings}</Link>
    </Nav.Item>
</Nav>
</div>
<div className="container">
<Route exact path="/" component={HomePage}></Route>
<Route path="/tictactoe" component={TicTacToe}></Route>
<Route path="/paddlegame" component={PaddleGame}></Route>
<Route path="/settings" component={Settings}></Route>
</div>
</Router>
      
        </>
      );
}
}
export default App;
