import React from 'react';
import './Settings.css';
import axios from 'axios';
import {ToggleButtonGroup, ToggleButton, ButtonToolbar} from 'react-bootstrap'
import lang from '../lang.json'
import { EventEmitter } from '../EventEmitter'
import { Alert } from 'antd'

class Settings extends React.Component {
  constructor() {
    super();
  }

  setLang(lang) {
    localStorage.setItem('lang', lang);
    EventEmitter.dispatch('langChange', true)
  }

  
  getLocation() {
    navigator.geolocation.getCurrentPosition(function(position) {
      console.log(position)
      axios.get('https://api.opencagedata.com/geocode/v1/json?q=50.2617926+19.0309843&key=f5217a495b934378b4a5e5b6532cb8e4')
      .then((resp) => {
        alert(resp.data.results[0].formatted);
      })
    });
  }

  render() {
    
    return (
        <>
        <h1>{lang[localStorage.getItem('lang')].settings}</h1>
          <div className="settings-element">
                <h3>{lang[localStorage.getItem('lang')].language}</h3>
                <ButtonToolbar>
                    <ToggleButtonGroup type="radio" name="options" defaultValue={1} >
                        <ToggleButton value={1} className="lang-changes" onClick={this.setLang.bind(this, 'pl')}>PL</ToggleButton>
                        <ToggleButton value={2} className="lang-changes" onClick={this.setLang.bind(this, 'en')}>EN</ToggleButton>
                    </ToggleButtonGroup>
                </ButtonToolbar>

          </div>

          <div className="settings-element">
            <h2>{lang[localStorage.getItem('lang')].location}</h2>
            <button className="localization-btn btn-primary" onClick={this.getLocation.bind(this)}>{lang[localStorage.getItem('lang')].locationBtn}</button>
          </div>
        </>
      );
}
}
export default Settings;
