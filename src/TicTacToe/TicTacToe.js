import React from 'react';
import './TicTacToe.css';
import { Modal, message, Button, Alert} from 'antd';

import 'antd/dist/antd.css';

class TicTacToe extends React.Component {
  constructor() {
      super();

      this.state = {
          player1: 'O',
          player2: 'X',
          turn: 0,
          board: [ 

              '', '', '',
              '', '', '',
              '', '', '',
          ],
          gameEnabled: true,
          modalVisible: false,
          winner: ''
      }
      this.computerTurn = this.computerTurn.bind(this);
  }

  checkGameStatus(slectedPlayer) {
    if (!this.state.gameEnabled) { return }

      for (let i = 0; i<=6; i = i+3) {
          if(!!this.state.board[i] && !!this.state.board[i+1] && !!this.state.board[i+2]) {
              if (this.state.board[i] === this.state.board[i+1] && this.state.board[i+1] === this.state.board[i+2] )
                  {this.endGame(slectedPlayer);}
              
          }
        }

      for (let i = 0; i < 3; i++) {
        if(!!this.state.board[i] && !!this.state.board[i+3] && !!this.state.board[i+6]) {
            if (this.state.board[i] === this.state.board[i+3] && this.state.board[i+3] ===this.state.board[i+6]) {
                this.endGame(slectedPlayer);
            }
        }
    }

        if(!!this.state.board[0] && !!this.state.board[4] && !!this.state.board[8]) {
            if (this.state.board[0] === this.state.board[4] && this.state.board[4] ===this.state.board[8]) {
                this.endGame(slectedPlayer);
            }
        }
    
        if(!!this.state.board[2] && !!this.state.board[4] && !!this.state.board[6]) {
            if (this.state.board[2] === this.state.board[4] && this.state.board[4] ===this.state.board[6]) {
                this.endGame(slectedPlayer);
            }
        } 

     if(this.state.gameEnabled && this.state.turn > 7) {
          this.endGame();
            return;
    }
        return;
  }

  getRandomInt() {
    let min = Math.ceil(0);
    let max = Math.floor(8);

    let randomInt = Math.floor(Math.random() * (max - min + 1)) + min;

    while (this.state.board[randomInt] !== '') {
      randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
    }

    return randomInt;
  }

  computerTurn() {
    let { board } = this.state

    function _getRandomInt() {
      let min = Math.ceil(0);
      let max = Math.floor(8);

      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    let computerFieldSelected = _getRandomInt();

    if (board[computerFieldSelected] === '') {
      board[computerFieldSelected] = 'o'
    } else if (this.state.gameEnabled && this.state.board.indexOf('') >= 0) {
      this.computerTurn();
      return;
    } else return;

    this.setState({
      turn: this.state.turn + 1,
      board
    })

    this.checkGameStatus('o');
  }

async onFieldClick(index) {
    if(!this.state.gameEnabled) {return};
    if(this.state.board[index] !== ''){alert('To miejsce jest już zajęte!'); return};
  
    let board = this.state.board;
    board[index] = 'x';
    
    this.setState({
        board,
        turn: this.state.turn + 1
    }, this.computerTurn)

    this.checkGameStatus('x');
}

resetGameBoard() {
    this.setState({
        board: [ 

            '', '', '',
            '', '', '',
            '', '', '',
        ],
        gameEnabled: true,
        turn: 0,
        modalVisible: false
    })
}

endGame(selectedPlayer) {
  
  this.setState({
        gameEnabled: false,
      })
      if (selectedPlayer && this.state.gameEnabled) {
        this.setState({
            winner: selectedPlayer,
            modalVisible: true
        })
      } else if (this.state.gameEnabled && this.state.turn > 7){
        console.log('REMIS! Żaden gracz nie wygrał!')
        return;
      }
      
}

checkIfFieldIsNotEmpty(field) {
    if (field.length > 0) {
      return 'game-board--field game-board--field--disabled';
    } else {
      return 'game-board--field graduation'
    }
  }

  handleOk = e => {
    this.setState({
        modalVisible: false,
    });
  };

   render() {  
    return (
    <>
        <div className="game-board-tic">
            { this.state.board.map((field, key) => {
                return (
                    <div className={this.checkIfFieldIsNotEmpty(field)}
                    key={key} 
                    onClick={this.onFieldClick.bind(this, key)}>
                        <div className="game-board--field-content">{field}</div>
                    </div>
                )
            }) }
        </div>
        <button onClick={this.resetGameBoard.bind(this)} className="btn btn-danger">Reset Game</button>
       
        <Modal
          visible={this.state.modalVisible}
          title="Zwycięstwo!!"
          onOk={this.handleOk}
          onCancel={this.handleOk}
          footer={[
            <Button key="submit" type="primary" onClick={this.handleOk}>
              Ok
            </Button>,
          ]}
        >
          <p>Wygrał: {this.state.winner}</p>
        </Modal>
    </>
    );
}
}

export default TicTacToe;
